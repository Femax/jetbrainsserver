package com.test.ktor

import com.test.ktor.controller.fileRoute
import com.test.ktor.websocket.webSocket
import io.ktor.application.Application
import io.ktor.application.install
import io.ktor.features.CORS
import io.ktor.features.ContentNegotiation
import io.ktor.gson.gson
import io.ktor.http.HttpMethod
import io.ktor.routing.routing
import org.koin.ktor.ext.Koin
import java.text.DateFormat

fun Application.mainModule() {
    install(ContentNegotiation) {
        gson {
            setDateFormat(DateFormat.LONG)
            setPrettyPrinting()
        }
    }
    install(Koin) {
        modules(context)
    }
    install(CORS) {
        anyHost()
        allowCredentials = true
        allowNonSimpleContentTypes = true
        methods.addAll(HttpMethod.DefaultMethods)
    }
    //Main router for http and websocket
    routing {
        fileRoute()
        webSocket()
    }
}
