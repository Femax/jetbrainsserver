package  com.test.ktor.controller

import com.test.ktor.Config.basePath
import com.test.ktor.service.FileService
import io.ktor.application.call
import io.ktor.response.respond
import io.ktor.routing.Routing
import io.ktor.routing.get
import org.koin.ktor.ext.inject


fun Routing.fileRoute() {
    val fileService: FileService by inject()
    get("/") {
        val path = call.request.queryParameters["path"] ?: "$basePath/"
        call.respond(fileService.getFileList(path))
    }
}
