package com.test.ktor.websocket

import com.google.gson.Gson
import com.test.ktor.Config
import com.test.ktor.service.FileService
import io.ktor.application.Application
import io.ktor.application.install
import io.ktor.http.cio.websocket.CloseReason
import io.ktor.http.cio.websocket.Frame
import io.ktor.http.cio.websocket.close
import io.ktor.http.cio.websocket.readText
import io.ktor.routing.routing
import io.ktor.websocket.WebSockets
import io.ktor.websocket.webSocket
import kotlinx.coroutines.launch
import org.koin.ktor.ext.inject
import java.time.Duration

fun Application.webSocket() {
    install(WebSockets) {
        pingPeriod = Duration.ofMinutes(1)
    }
    val fileService: FileService by inject()
    val gson: Gson by inject()
    routing {
        webSocket("/watchDirectory") {
            for (frame in incoming) {
                when (frame) {
                    is Frame.Text -> {
                        val pathParam = frame.readText()
                        val path = if (pathParam == "/") Config.basePath.toString() + "/" else pathParam
                        send(
                            Frame.Text(
                                gson.toJson(fileService
                                    .profilePath(path)
                                    .sortedBy { -it.file.size })
                            )
                        )
                        fileService
                            .watchDirectory(path)
                            ?.map {
                                fileService
                                    .profilePath(path)
                                    .sortedBy { -it.file.size }
                            }
                            ?.subscribe {
                                launch {
                                    send(Frame.Text(gson.toJson(it)))
                                }
                            }
                        if (path.equals("bye", ignoreCase = true)) {
                            close(CloseReason(CloseReason.Codes.NORMAL, "Client said BYE"))
                        }
                    }
                }
            }
        }
        webSocket("/profilePath") {
            for (frame in incoming) {
                when (frame) {
                    is Frame.Text -> {
                        val path = frame.readText()
                        launch {
                            send(
                                Frame.Text(
                                    gson.toJson(fileService
                                        .profilePath(path)
                                        .sortedBy { -it.file.size })
                                )
                            )
                        }
                        if (path.equals("bye", ignoreCase = true)) {
                            close(CloseReason(CloseReason.Codes.NORMAL, "Client said BYE"))
                        }
                    }
                }
            }
        }
    }
}
