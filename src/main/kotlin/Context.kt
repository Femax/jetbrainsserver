package com.test.ktor

import com.google.gson.Gson
import com.test.ktor.service.FileService
import com.test.ktor.service.FileServiceImpl
import org.koin.dsl.module
import org.koin.experimental.builder.singleBy
import java.nio.file.FileSystems

val context = module(createdAtStart = true) {
    single {
        FileSystems.getDefault().newWatchService()
    }
    singleBy<FileService, FileServiceImpl>()
    single { Gson() }
}
